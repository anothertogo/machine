from flask import Flask
import premade_estimator as pe

application = Flask(__name__)

@application.route('/robot', methods=['POST'])
def temp():
    pe.main("C:/Users/isaac.gutierrez/Desktop/MLRobot/RobotConServicio/premade_estimator.py")
    return pe.var

if __name__ == "__main__":
    application.run()